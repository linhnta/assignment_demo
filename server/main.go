package main

import (
	"context"
	"log"

	"google.golang.org/grpc"

	"net"

	pb "gitlab.com/linhnta/assignment_demo/proto"
)

const (
	port = ":50051"
)

var users []*pb.UserInfo

type UserServer struct {
	pb.UnimplementedUserServer
}

func main() {
	initUsers()
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	pb.RegisterUserServer(s, &UserServer{})

	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func initUsers() {
	user1 := &pb.UserInfo{Id: 1, Name: "Linh",
		Age: 21, Address: "ktx", PhoneNumber: "0398667273"}
	user2 := &pb.UserInfo{Id: 2, Name: "Ngan",
		Age: 21, Address: "ktx", PhoneNumber: "0398667243"}

	users = append(users, user1)
	users = append(users, user2)
}

func (s *UserServer) ListUsers(ctx context.Context, in *pb.Empty) (*pb.UserResponse, error) {
    var res pb.UserResponse;
	res.UserInfos = users
	  return &res , nil;	
}

func (s *UserServer) CreateUser(ctx context.Context,
	in *pb.UserInfo) (*pb.UserResponse, error) {
	log.Printf("Received: %v", in)
	var res pb.UserResponse;
	users = append(users, in)
	res.UserInfos = users
	return &res, nil
}

func (s *UserServer) UpdateUser(ctx context.Context,
	in *pb.UserInfo) (*pb.UserResponse, error) {
	log.Printf("Received: %v", in)

	var res pb.UserResponse;
	for index, user := range users {
		if user.GetId() == in.GetId(){
			users = append(users[:index], users[index+1:]...)
			in.Id = user.GetId();
			users = append(users, in);
			res.UserInfos =users;
		}
	}
	return &res, nil
}

func (s *UserServer) DeleteUser(ctx context.Context,
	in *pb.Id) (*pb.UserResponse, error) {
	log.Printf("Received: %v", in)

	var res pb.UserResponse;
	
	for index, user := range users {
		if user.GetId() == in.GetId() {
			users = append(users[:index], users[index+1:]...)
			res.UserInfos =users;
			break
		}
		
	}	

	return &res, nil
}
